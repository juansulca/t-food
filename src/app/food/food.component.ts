import { Component, OnInit } from '@angular/core';
import { DbService } from '../db.service';

@Component({
  selector: 'app-food',
  templateUrl: './food.component.html',
  styleUrls: ['./food.component.css']
})
export class FoodComponent implements OnInit {

  food = [];
  searchString: string;

  constructor(private db: DbService) { }

  ngOnInit() {
    this.food = this.db.getFood();
  }

  removeFood(id: number){
    this.db.deleteFood(id);
  }

  search(){
    if(this.searchString.length == 0)
      this.food = this.db.getFood();
    else
      this.food = this.db.searcFood(this.searchString);
  }
}
