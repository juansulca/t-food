import { Component, OnInit } from '@angular/core';
import { DbService } from '../db.service';
import { Supplier } from '../Supplier';

@Component({
  selector: 'app-suppliers',
  templateUrl: './suppliers.component.html',
  styleUrls: ['./suppliers.component.css']
})
export class SuppliersComponent implements OnInit {

  suppliers: Supplier[];
  searchString: string;

  constructor(private db: DbService) { }

  ngOnInit() {
    this.suppliers = this.db.getSuppliers();
  }

  removeSupplier(id: number){
    this.db.deleteSupplier(id);
  }

  search(){
    if(this.searchString.length == 0)
      this.suppliers = this.db.getSuppliers();
    else
      this.suppliers = this.db.searchSupplier(this.searchString);
  }

}
