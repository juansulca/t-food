export class Chef {
    id: number;
    name: string;
    review: string;
    stars: number;
}