import { Injectable } from '@angular/core';
import { Food } from './Food';
import { Chef } from './Chef';
import { Supplier } from './Supplier';

@Injectable({
  providedIn: 'root'
})
export class DbService {
  
  private idCounter = 1;
  private chefId = 1;
  private supplierId = 1;

  chefs: Chef[] = [
    {
      id: 1,
      name: "Gordon Ramsey",
      review: "Great!",
      stars: 5
    }
  ];

  food: Food[] = [
    {
      id: 1,
      name: "Hornado",
      description: "Cooked pork",
      price: 3.50
    }
  ];

  suppliers: Supplier[] = [
    {
      id: 1,
      name: "La favorita",
      phone: "0999999334",
      rating: 4.5
    }
  ];

  constructor() {  }

  getFood(): Food[]{
    return this.food;
  }

  getFoodById(id: number): Food{
    return this.food.find(function (f) {return f.id == id});
  }

  updateFood(newFood: Food){
    let index = this.food.findIndex(function(f) {return f.id == newFood.id});
    if (index > -1)
      this.food[index] = newFood;
  }

  insertFood(newFood: Food){
    newFood.id = ++this.idCounter;
    this.food.push(newFood);
  }

  deleteFood(id: number){
    let index = this.food.findIndex(function (f) { return f.id == id });
    if (index > -1)
      this.food.splice(index, 1);
  }

  searcFood(name: string){
    return this.food.filter(function(f){ return !f.name.search(name) })
  }

  getChefs(): Chef[]{
    return this.chefs;
  }

  getChefById(id: number): Chef{
    return this.chefs.find(function(c){
      return c.id == id;
    });
  }

  insertChef(chef: Chef){
    chef.id = ++this.chefId;
    this.chefs.push(chef);
  }

  updateChef(chef: Chef){
    let index = this.chefs.findIndex(function(c) { 
      return c.id == chef.id
    });
    if (index > -1)
      this.chefs[index] = chef;
  }

  deleteChef(id: number) {
    let index = this.chefs.findIndex(function (c) { return c.id == id });
    if (index > -1)
      this.chefs.splice(index, 1);
  }

  searchChefs(name: string){
    return this.chefs.filter(function(c){ return !c.name.search(name) })
  }

  getSuppliers(){
    return this.suppliers;
  }

  getSupplierById(id: number){
    return this.suppliers.find(function(s){
      return s.id == id;
    });
  }

  insertSupplier(supplier: Supplier){
    supplier.id = ++this.supplierId;
    this.suppliers.push(supplier);
  }

  updateSupplier(supplier: Supplier) {
    let index = this.suppliers.findIndex(function (s) {
      return s.id == supplier.id
    });
    if (index > -1)
      this.suppliers[index] = supplier;
  }

  deleteSupplier(id: number) {
    let index = this.suppliers.findIndex(function (s) { return s.id == id });
    if (index > -1)
      this.suppliers.splice(index, 1);
  }

  searchSupplier(name: string){
    return this.suppliers.filter(function(s){ return !s.name.search(name) })
  }
}
