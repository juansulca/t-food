import { Component, OnInit } from '@angular/core';
import { DbService } from '../db.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Supplier } from '../Supplier';

@Component({
  selector: 'app-edit-supplier',
  templateUrl: './edit-supplier.component.html',
  styleUrls: ['./edit-supplier.component.css']
})
export class EditSupplierComponent implements OnInit {

  id: number;
  supplier: Supplier = new Supplier();

  constructor(private db: DbService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get("id");
    if (id) {
      this.id = +id;
      this.supplier = this.db.getSupplierById(this.id);
    }
  }

  saveSupplier() {
    if (this.id) {
      this.supplier.id = this.id;
      this.db.updateSupplier(this.supplier);
    } else {
      this.db.insertSupplier(this.supplier);
    }
    this.router.navigateByUrl('/suppliers');
  }

}
