import { Component, OnInit } from '@angular/core';
import { DbService } from '../db.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Food } from '../Food';

@Component({
  selector: 'app-new-dish',
  templateUrl: './new-dish.component.html',
  styleUrls: ['./new-dish.component.css']
})
export class NewDishComponent implements OnInit {

  id:number;
  dish: Food = new Food();
  
  constructor(private db: DbService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get("id");
    if(id){
      this.id = +id;
      this.dish = this.db.getFoodById(this.id);
    }
  }

  saveDish(){
    if(this.id){
      this.dish.id = this.id;
      this.db.updateFood(this.dish);
    } else{
      this.db.insertFood(this.dish);
    }
    this.router.navigateByUrl('/food');
  }

}
