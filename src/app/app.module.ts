import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NavbarComponent } from './navbar/navbar.component';
import { FoodComponent } from './food/food.component';
import { ChefsComponent } from './chefs/chefs.component';
import { SuppliersComponent } from './suppliers/suppliers.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { NewDishComponent } from './new-dish/new-dish.component';
import { DbService } from './db.service';
import { EditSupplierComponent } from './edit-supplier/edit-supplier.component';
import { EditChefComponent } from './edit-chef/edit-chef.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    FoodComponent,
    ChefsComponent,
    SuppliersComponent,
    WelcomeComponent,
    NewDishComponent,
    EditSupplierComponent,
    EditChefComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],
  providers: [DbService],
  bootstrap: [AppComponent]
})
export class AppModule { }
