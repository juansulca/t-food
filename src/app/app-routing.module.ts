import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ChefsComponent } from './chefs/chefs.component';
import { FoodComponent } from './food/food.component';
import { SuppliersComponent } from './suppliers/suppliers.component';
import { WelcomeComponent } from './welcome/welcome.component';
import { NewDishComponent } from './new-dish/new-dish.component';
import { EditChefComponent } from './edit-chef/edit-chef.component';
import { EditSupplierComponent } from './edit-supplier/edit-supplier.component';


const routes: Routes = [
  { path: '', component: WelcomeComponent },
  { path: 'food', component: FoodComponent },
  { path: 'chefs', component: ChefsComponent },
  { path: 'suppliers', component: SuppliersComponent },
  { path: 'new-dish', component: NewDishComponent },
  { path: 'new-dish/:id', component: NewDishComponent },
  { path: 'edit-chef', component: EditChefComponent },
  { path: 'edit-chef/:id', component: EditChefComponent },
  { path: 'edit-supplier', component: EditSupplierComponent },
  { path: 'edit-supplier/:id', component: EditSupplierComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
