import { Component, OnInit } from '@angular/core';
import * as mapboxgl from 'mapbox-gl';

@Component({
  selector: 'app-welcome',
  templateUrl: './welcome.component.html',
  styleUrls: ['./welcome.component.css']
})
export class WelcomeComponent implements OnInit {

  mapbox: any;
  map: any;

  constructor() { }

  ngOnInit() {
    this.mapbox = mapboxgl;
    this.mapbox.accessToken = "pk.eyJ1IjoianVhbnN1bGNhIiwiYSI6ImNqcmZpY2JyajI5Mmo0OXA4Zm1xNTVkeXMifQ.2g8vutBtGBu-NlzkpREnPQ";
    this.loadMap();
    this.putMarker();
  }

  loadMap(){
    this.map = new this.mapbox.Map({
      container: 'map',
      style: 'mapbox://styles/mapbox/streets-v11',
      zoom: 14,
      center: [-78.481715, -0.181683]
    });
  }

  putMarker(){
    var marker = new this.mapbox.Marker()
    .setLngLat([-78.481715, -0.181683])
    .addTo(this.map);
  }
}
