import { Component, OnInit } from '@angular/core';
import { DbService } from '../db.service';
import { Router, ActivatedRoute } from '@angular/router';
import { Chef } from '../Chef';

@Component({
  selector: 'app-edit-chef',
  templateUrl: './edit-chef.component.html',
  styleUrls: ['./edit-chef.component.css']
})
export class EditChefComponent implements OnInit {

  id: number;
  chef: Chef = new Chef();

  constructor(private db: DbService, private router: Router, private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    let id = this.activatedRoute.snapshot.paramMap.get("id");
    if (id) {
      this.id = +id;
      this.chef = this.db.getChefById(this.id);
    }
  }

  saveChef(){
    if (this.id) {
      this.chef.id = this.id;
      this.db.updateChef(this.chef);
    } else {
      this.db.insertChef(this.chef);
    }
    this.router.navigateByUrl('/chefs');
  }

}
