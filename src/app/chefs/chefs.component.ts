import { Component, OnInit } from '@angular/core';
import { DbService } from '../db.service';
import { Chef } from '../Chef';

@Component({
  selector: 'app-chefs',
  templateUrl: './chefs.component.html',
  styleUrls: ['./chefs.component.css']
})
export class ChefsComponent implements OnInit {

  chefs:Chef[] = [];
  searchString: string;

  constructor(private db: DbService) { }

  ngOnInit() {
    this.chefs = this.db.getChefs();
  }

  removeChef(id: number) {
    this.db.deleteChef(id);
  }

  search(){
    if(this.searchString.length == 0)
      this.chefs = this.db.getChefs();
    else
      this.chefs = this.db.searchChefs(this.searchString);
  }

}
