export class Supplier {
    id: number;
    name: string;
    phone: string;
    rating: number;
}